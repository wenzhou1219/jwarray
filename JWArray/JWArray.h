/********************************************************************
	创建日期:	2014/04/12
	包含文件:	JWArray.h
	  创建者:	Jim Wen
        版本:	V1.3
	
		功能:	实现了顺序存储结构的线性表、栈、队列
		说明:	1.V1.3相对V1.2实现了泛型
				2.线性表的索引从0开始
				3.栈的栈底为索引为0，(长度-1)为栈顶索引
				4.队列头索引为0，(长度-1)为队列尾索引
				5.这里的泛型支持就是对每种类型实现一次
				6.自定义类型时，首先更改JWArrayElem定义，然后复制一个已有的类型如int，替换_int即可
				  要注意JWArrayPrintfElem、JWArrayDump、JWArray_CompareFunc、JWArray_TraverseFunc
				  的细微变化
				7.JWArrayElem的数据元素类型是字符串或包括字符串的结构体时，还
				  要对赋值和移动操作有些改变--如=要换成strlen函数，一般不使用
				  字符串作为数据元素，一旦使用要注意!!!。
*********************************************************************/

#ifndef JWARRAY_H_H_H
#define JWARRAY_H_H_H

#include "JWArray_base.h"
#include "JWArray_int.h"
#include "JWArray_char.h"
#include "JWArray_double.h"
#include "JWArray_float.h"

#define JWArrayElem(type)		JWArrayElem_##type
#define JWArray(type)			JWArray_##type

//基础线性表函数声明 
#define JWArrayCreate(type)		JWArrayCreate_##type
#define JWArrayDestroy(type)	JWArrayDestroy_##type
#define JWArrayMakeEmpty(type)	JWArrayMakeEmpty_##type

#define JWArrayIsEmpty(type)	JWArrayIsEmpty_##type
#define JWArrayIsFull(type)		JWArrayIsFull_##type
#define JWArrayGetLength(type)	JWArrayGetLength_##type

#define JWArrayGetAt(type)		JWArrayGetAt_##type
#define JWArraySetAt(type)		JWArraySetAt_##type
#define JWArrayInsert(type)		JWArrayInsert_##type
#define JWArrayOrderInsert(type)	JWArrayOrderInsert_##type
#define JWArrayDelete(type)		JWArrayDelete_##type
#define JWArrayLocate(type)		JWArrayLocate_##type
#define JWArrayTraverse(type)	JWArrayTraverse_##type

#define JWArrayPrintfElem(type)	JWArrayPrintfElem_##type
#define JWArrayDump(type)		JWArrayDump_##type

#define JWArray_CompareFunc(type)	JWArray_CompareFunc_##type
#define JWArray_TraverseFunc(type)	JWArray_TraverseFunc_##type

//栈操作函数声明	
#define JWArrayGetTop(type)		JWArrayGetTop_##type
#define JWArraySetTop(type)		JWArraySetTop_##type
#define JWArrayPush(type)		JWArrayPush_##type
#define JWArrayPop(type)		JWArrayPop_##type

//队列操作函数声明
#define JWArrayGetHead(type)	JWArrayGetHead_##type
#define JWArraySetHead(type)	JWArraySetHead_##type
#define JWArrayEnQueue(type)	JWArrayEnQueue_##type
#define JWArrayDeQueue(type)	JWArrayDeQueue_##type

#endif