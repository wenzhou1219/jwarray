#ifndef JWARRAY_CHAR_H_H_H
#define JWARRAY_CHAR_H_H_H

#include "JWArray_base.h"

typedef char			JWArrayElem_char;

typedef struct
{
	JWArrayElem_char	*pElem;		//数据元素存储区域
	int				nLength;		//当前数据元素长度
	int				nTotalSize;		//当前线性表长度
	int				nIncrSize;		//当线性表已满时，重新分配的新增区域大小
}JWArray_char;

typedef JWArray_BOOL (*JWArray_CompareFunc_char)(JWArrayElem_char elem1, JWArrayElem_char elem2);
typedef JWArray_BOOL (*JWArray_TraverseFunc_char)(JWArrayElem_char *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基础线性表函数声明                                                           */
	/************************************************************************/
	JWArray_char* JWArrayCreate_char(const int nInitSize, const int nIncrSize);
	void JWArrayDestroy_char(JWArray_char *pArray);
	void JWArrayMakeEmpty_char(JWArray_char *pArray );

	JWArray_BOOL JWArrayIsEmpty_char(JWArray_char *pArray);
	JWArray_BOOL JWArrayIsFull_char(JWArray_char *pArray);
	int JWArrayGetLength_char(JWArray_char *pArray);

	JWArray_BOOL JWArrayGetAt_char(JWArray_char *pArray, const int nIndex, JWArrayElem_char *pElem);
	JWArray_BOOL JWArraySetAt_char(JWArray_char *pArray, const int nIndex, const JWArrayElem_char elem);
	JWArray_BOOL JWArrayInsert_char(JWArray_char *pArray, const int nIndex, const JWArrayElem_char elem);
	JWArray_BOOL JWArrayOrderInsert_char(JWArray_char *pArray, const JWArrayElem_char elem, JWArray_CompareFunc_char pCompare);
	JWArray_BOOL JWArrayDelete_char(JWArray_char *pArray, const int nIndex, JWArrayElem_char *pElem);
	int JWArrayLocate_char(JWArray_char *pArray, const JWArrayElem_char elem, JWArray_CompareFunc_char pCompare);
	void JWArrayTraverse_char(JWArray_char *pArray, JWArray_TraverseFunc_char pTraverse);

	JWArray_BOOL JWArrayPrintfElem_char(JWArrayElem_char *pElem);
	void JWArrayDump_char(JWArray_char *pArray);

	/************************************************************************/
	/* 栈操作函数声明															*/
	/************************************************************************/
	JWArray_BOOL JWArrayGetTop_char(JWArray_char *pArray, JWArrayElem_char *pElem);
	JWArray_BOOL JWArraySetTop_char(JWArray_char *pArray, const JWArrayElem_char elem);
	JWArray_BOOL JWArrayPush_char(JWArray_char *pArray, const JWArrayElem_char elem);
	JWArray_BOOL JWArrayPop_char(JWArray_char *pArray, JWArrayElem_char *pElem);

	/************************************************************************/
	/* 队列操作函数声明													    */
	/************************************************************************/
	JWArray_BOOL JWArrayGetHead_char(JWArray_char *pArray, JWArrayElem_char *pElem);
	JWArray_BOOL JWArraySetHead_char(JWArray_char *pArray, const JWArrayElem_char elem);
	JWArray_BOOL JWArrayEnQueue_char(JWArray_char *pArray, const JWArrayElem_char elem);
	JWArray_BOOL JWArrayDeQueue_char(JWArray_char *pArray, JWArrayElem_char *pElem);

#ifdef __cplusplus
};
#endif

#endif