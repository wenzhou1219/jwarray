#ifndef JWARRAY_FLOAT_H_H_H
#define JWARRAY_FLOAT_H_H_H

#include "JWArray_base.h"

typedef float				JWArrayElem_float;

typedef struct
{
	JWArrayElem_float	*pElem;			//数据元素存储区域
	int				nLength;		//当前数据元素长度
	int				nTotalSize;		//当前线性表长度
	int				nIncrSize;		//当线性表已满时，重新分配的新增区域大小
}JWArray_float;

typedef JWArray_BOOL (*JWArray_CompareFunc_float)(JWArrayElem_float elem1, JWArrayElem_float elem2);
typedef JWArray_BOOL (*JWArray_TraverseFunc_float)(JWArrayElem_float *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基础线性表函数声明                                                           */
	/************************************************************************/
	JWArray_float* JWArrayCreate_float(const int nInitSize, const int nIncrSize);
	void JWArrayDestroy_float(JWArray_float *pArray);
	void JWArrayMakeEmpty_float(JWArray_float *pArray );

	JWArray_BOOL JWArrayIsEmpty_float(JWArray_float *pArray);
	JWArray_BOOL JWArrayIsFull_float(JWArray_float *pArray);
	int JWArrayGetLength_float(JWArray_float *pArray);

	JWArray_BOOL JWArrayGetAt_float(JWArray_float *pArray, const int nIndex, JWArrayElem_float *pElem);
	JWArray_BOOL JWArraySetAt_float(JWArray_float *pArray, const int nIndex, const JWArrayElem_float elem);
	JWArray_BOOL JWArrayInsert_float(JWArray_float *pArray, const int nIndex, const JWArrayElem_float elem);
	JWArray_BOOL JWArrayOrderInsert_float(JWArray_float *pArray, const JWArrayElem_float elem, JWArray_CompareFunc_float pCompare);
	JWArray_BOOL JWArrayDelete_float(JWArray_float *pArray, const int nIndex, JWArrayElem_float *pElem);
	int JWArrayLocate_float(JWArray_float *pArray, const JWArrayElem_float elem, JWArray_CompareFunc_float pCompare);
	void JWArrayTraverse_float(JWArray_float *pArray, JWArray_TraverseFunc_float pTraverse);

	JWArray_BOOL JWArrayPrintfElem_float(JWArrayElem_float *pElem);
	void JWArrayDump_float(JWArray_float *pArray);

	/************************************************************************/
	/* 栈操作函数声明															*/
	/************************************************************************/
	JWArray_BOOL JWArrayGetTop_float(JWArray_float *pArray, JWArrayElem_float *pElem);
	JWArray_BOOL JWArraySetTop_float(JWArray_float *pArray, const JWArrayElem_float elem);
	JWArray_BOOL JWArrayPush_float(JWArray_float *pArray, const JWArrayElem_float elem);
	JWArray_BOOL JWArrayPop_float(JWArray_float *pArray, JWArrayElem_float *pElem);

	/************************************************************************/
	/* 队列操作函数声明													    */
	/************************************************************************/
	JWArray_BOOL JWArrayGetHead_float(JWArray_float *pArray, JWArrayElem_float *pElem);
	JWArray_BOOL JWArraySetHead_float(JWArray_float *pArray, const JWArrayElem_float elem);
	JWArray_BOOL JWArrayEnQueue_float(JWArray_float *pArray, const JWArrayElem_float elem);
	JWArray_BOOL JWArrayDeQueue_float(JWArray_float *pArray, JWArrayElem_float *pElem);

#ifdef __cplusplus
};
#endif

#endif