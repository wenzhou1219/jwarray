#ifndef JWARRAY_INT_H_H_H
#define JWARRAY_INT_H_H_H

#include "JWArray_base.h"

typedef int				JWArrayElem_int;

typedef struct
{
	JWArrayElem_int	*pElem;			//数据元素存储区域
	int				nLength;		//当前数据元素长度
	int				nTotalSize;		//当前线性表长度
	int				nIncrSize;		//当线性表已满时，重新分配的新增区域大小
}JWArray_int;

typedef JWArray_BOOL (*JWArray_CompareFunc_int)(JWArrayElem_int elem1, JWArrayElem_int elem2);
typedef JWArray_BOOL (*JWArray_TraverseFunc_int)(JWArrayElem_int *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基础线性表函数声明                                                           */
	/************************************************************************/
	JWArray_int* JWArrayCreate_int(const int nInitSize, const int nIncrSize);
	void JWArrayDestroy_int(JWArray_int *pArray);
	void JWArrayMakeEmpty_int(JWArray_int *pArray );

	JWArray_BOOL JWArrayIsEmpty_int(JWArray_int *pArray);
	JWArray_BOOL JWArrayIsFull_int(JWArray_int *pArray);
	int JWArrayGetLength_int(JWArray_int *pArray);

	JWArray_BOOL JWArrayGetAt_int(JWArray_int *pArray, const int nIndex, JWArrayElem_int *pElem);
	JWArray_BOOL JWArraySetAt_int(JWArray_int *pArray, const int nIndex, const JWArrayElem_int elem);
	JWArray_BOOL JWArrayInsert_int(JWArray_int *pArray, const int nIndex, const JWArrayElem_int elem);
	JWArray_BOOL JWArrayOrderInsert_int(JWArray_int *pArray, const JWArrayElem_int elem, JWArray_CompareFunc_int pCompare);
	JWArray_BOOL JWArrayDelete_int(JWArray_int *pArray, const int nIndex, JWArrayElem_int *pElem);
	int JWArrayLocate_int(JWArray_int *pArray, const JWArrayElem_int elem, JWArray_CompareFunc_int pCompare);
	void JWArrayTraverse_int(JWArray_int *pArray, JWArray_TraverseFunc_int pTraverse);

	JWArray_BOOL JWArrayPrintfElem_int(JWArrayElem_int *pElem);
	void JWArrayDump_int(JWArray_int *pArray);

	/************************************************************************/
	/* 栈操作函数声明															*/
	/************************************************************************/
	JWArray_BOOL JWArrayGetTop_int(JWArray_int *pArray, JWArrayElem_int *pElem);
	JWArray_BOOL JWArraySetTop_int(JWArray_int *pArray, const JWArrayElem_int elem);
	JWArray_BOOL JWArrayPush_int(JWArray_int *pArray, const JWArrayElem_int elem);
	JWArray_BOOL JWArrayPop_int(JWArray_int *pArray, JWArrayElem_int *pElem);

	/************************************************************************/
	/* 队列操作函数声明													    */
	/************************************************************************/
	JWArray_BOOL JWArrayGetHead_int(JWArray_int *pArray, JWArrayElem_int *pElem);
	JWArray_BOOL JWArraySetHead_int(JWArray_int *pArray, const JWArrayElem_int elem);
	JWArray_BOOL JWArrayEnQueue_int(JWArray_int *pArray, const JWArrayElem_int elem);
	JWArray_BOOL JWArrayDeQueue_int(JWArray_int *pArray, JWArrayElem_int *pElem);

#ifdef __cplusplus
};
#endif

#endif